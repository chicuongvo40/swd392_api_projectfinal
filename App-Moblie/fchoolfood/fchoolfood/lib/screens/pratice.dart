import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/screens/page2.dart';

class MainFoodPage extends StatefulWidget {
  static String routeName = "/practice";

  const MainFoodPage({Key? key}) : super(key: key);

  @override
  State<MainFoodPage> createState() => _MainFoodPageState();
}

class _MainFoodPageState extends State<MainFoodPage> {
  List<String> pic = ["assets/image/anime.jpg",
    "assets/image/anime1.jpg",
    "assets/image/anime3.jpg",
    "assets/image/anime4.jpg",
    "assets/image/anime5.png",

  ];


  PageController pageController = PageController(viewportFraction: 0.85);
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 45, bottom: 15),
            padding: EdgeInsets.only(left: 20,right: 20),
            child: Container(
              child:Row(
                children: [
                  Center(
                    child: Container(
                      width: 45,
                      height: 45,
                      child: Icon(Icons.arrow_back, color: Colors.white,),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color:Colors.greenAccent,
                      ),
                    ),
                  ),
                  SizedBox(width: 5,),
                  Expanded(
                    child: Container(
                      height: 45,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(
                          color: Colors.black26, // Màu sắc của đường viền
                          width: 1, // Độ dày của đường viền
                        ),

                      ),
                    ),
                  ),
                  SizedBox(width: 5,),
                  Center(
                    child: Container(
                      width: 45,
                      height: 45,
                      child: Icon(Icons.search, color: Colors.white,),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color:Colors.greenAccent,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          // Container(
          //   height: 320,
          //   child: PageView.builder(
          //       controller: pageController,
          //       itemCount: 5,
          //       itemBuilder: (context, position){
          //         return Stack(
          //           children: [
          //             Container(
          //               height: 220,
          //               margin: EdgeInsets.only(left: 5, right:5),
          //               decoration: BoxDecoration(
          //                   borderRadius: BorderRadius.circular(30),
          //                   color: position.isEven?Colors.greenAccent:Colors.amberAccent,
          //                   image: DecorationImage(
          //                       fit: BoxFit.cover,
          //                       image: AssetImage(
          //                           "assets/image/logo2.PNG"
          //                       )
          //                   )
          //               ),
          //             ),
          //             Align(
          //               alignment: Alignment.bottomCenter,
          //               child: Container(
          //                 height: 150,
          //                 margin: EdgeInsets.only(left: 30, right:30),
          //                 decoration: BoxDecoration(
          //                     borderRadius: BorderRadius.circular(30),
          //                     color: Colors.white,
          //                 ),
          //                 child: Container(
          //                   padding: EdgeInsets.only(top : 10, left: 15, right: 14),
          //                   child: Column(
          //                     children: [
          //                       Text("${position + 1} VIEW")
          //                     ],
          //                   ),
          //                 ),
          //               ),
          //
          //             )
          //           ],
          //         );
          //       }
          //   ),
          // ),
          // SizedBox(height: 5,),
          // Container(
          //   margin: EdgeInsets.only(left: 30),
          //   child: Text("List Item",
          //       style: TextStyle(fontSize: 20.0,)
          //   ),
          // ),
          Container(
            child: Column(
              children: [
                    Text("Võ Chí Cường",
                    style: TextStyle(
                      fontSize: 30,
                    ),),
                    Text("SE150674",
                      style: TextStyle(
                        fontSize: 20,
                      ),),
                    Text("List Item",
                  style: TextStyle(
                    fontSize: 20,
                  ),),
              ],
            ),
          ),
          SizedBox(height: 10,),
          Expanded(child: SingleChildScrollView(
            child: Container(
                height: 1500,
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: 5,
                    itemBuilder: (context,index){
                      return InkWell(
                        onTap:(){
                          Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => PageLog()))
                          ;
                        } ,
                        child: Column(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                  left: 20,right: 20
                              ),
                              child: Row(
                                children: [
                                  Container(
                                    width : 250,
                                    height: 150,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(20),
                                        color: Colors.lightBlueAccent,
                                        image: DecorationImage(
                                            fit: BoxFit.cover,
                                            image: AssetImage(pic[index])
                                        )
                                    ),
                                  ),
                                  Expanded(child: Container(
                                    height: 100,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(20),
                                        bottomRight: Radius.circular(20),
                                      ),
                                      color: Colors.white10,
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 10),
                                      child: Column(
                                        children: [
                                          Text("Item ${index + 1} ",
                                            style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                            ),

                                          )
                                        ],
                                      ),
                                    ),

                                  )),
                                ],
                              ),

                            ),
                            SizedBox(height: 10,),
                          ],
                        ) ,
                      );

                    }
                )
            )
          ))
        ],
      ),
    );
  }
}
