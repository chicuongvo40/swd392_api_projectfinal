import 'package:fchoolfood/view_404/404.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/data/notifiers/notifiers.dart';
import 'components/body.dart';

class SignInScreen extends StatefulWidget {
  static String routeName = "/sign_in";
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  @override
  void initState() {
    super.initState();
  }

  void parentMethod() {
    setState(() {
      print("Da chay setSate");
    });
  }

  @override
  Widget build(BuildContext context) {
   return ValueListenableBuilder(
      valueListenable: isConnectedNotifier,
      builder: (context, isConnected, child) {
        return isConnected ? Scaffold(
          body: Body(parentMethod: parentMethod ),
        ) : page404();
      },
    ) ;
  }
}



