import 'package:fchoolfood/home_screen.dart';
import 'package:fchoolfood/provider/sign_in_provider.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/customNavBar.dart';
import 'package:provider/provider.dart';
import 'profile_menu.dart';
import 'profile_pic.dart';

class Body extends StatelessWidget {


  const Body({
    Key? key
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    late bool _isSignedIn;
    _isSignedIn = context.read<SignInProvider>().isSignedIn;
    print(_isSignedIn);
    return 
      SafeArea(
        child: Stack(
        children: [
          Material(
            child: SingleChildScrollView(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: Column(
                children: [
                  SizedBox(height: 45,),
                  ProfilePic(),
                  SizedBox(height: 35),
                  ProfileMenu(
                    text: "My Account ",
                    icon: "assets/icons/User Icon.svg",
                    press: () => {},
                  ),
                  SizedBox(height: 15),
                  ProfileMenu(
                    text: "Notifications",
                    icon: "assets/icons/Bell.svg",
                    press: () {},
                  ),
                  SizedBox(height: 15),
                  ProfileMenu(
                    text: "Settings",
                    icon: "assets/icons/Settings.svg",
                    press: () {},
                  ),
                  SizedBox(height: 15),
                  ProfileMenu(
                    text: "Help Center",
                    icon: "assets/icons/Question mark.svg",
                    press: () {},
                  ),
                  SizedBox(height: 15),
                  ProfileMenu(
                    text: "Log Out",
                    icon: "assets/icons/Log out.svg",
                    press: () {
                      context.read<SignInProvider>().userSignOut();
                      var prefs;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => HomeScreen(prefs: prefs)),
                      );
                    },
                  ),
                  SizedBox(height: 105,)
                ],
              ),
            ),
          ),
          Positioned(
              bottom: 0,
              left: 0,
              child: CustomNavBar(
                key: ValueKey('someValue'),
                profile: true,
              )
          )
        ],
    ),
      );
  }
}
