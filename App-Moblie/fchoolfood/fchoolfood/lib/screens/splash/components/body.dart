import 'package:fchoolfood/home_screen.dart';
import 'package:fchoolfood/screens/sign_in/sign_in_screen.dart';
import 'package:fchoolfood/size_config.dart';
import 'package:fchoolfood/view_404/404.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/constants.dart';
import 'package:provider/provider.dart';
import 'package:fchoolfood/provider/sign_in_provider.dart';

// This is the best practice
import '../components/splash_content.dart';
import '../../../components/default_button.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  late bool _isSignedIn;
  @override
  void initState() {
    _isSignedIn = context.read<SignInProvider>().isSignedIn;
    super.initState();
  }

  int currentPage = 0;
  List<Map<String, String>> splashData = [
    {
      "text": "Welcome to FFood, Let’s shop!",
      "image": "assets/image/F-Food.png"

    },
    {
      "text":
          "We help people conect with store \naround United State of America",
      "image": "assets/image/logo.png"
    },
    {
      "text": "We show the easy way to shop. \nJust stay at home with us",
      "image": "assets/image/logo2.PNG"
    },
  ];
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: PageView.builder(
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: splashData.length,
                itemBuilder: (context, index) => SplashContent(
                  image: splashData[index]["image"],
                  text: splashData[index]['text'],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    Spacer(flex: 3),
                    DefaultButton(
                      text: "Continue",
                      press: () {
                        var prefs;
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  HomeScreen(prefs: prefs)),
                        );
                        // _isSignedIn == false ?
                        // Navigator.pushNamed(context, SignInScreen.routeName):
                        // Navigator.pushNamed(context, SignInScreen.routeName);
                        
                      },
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
