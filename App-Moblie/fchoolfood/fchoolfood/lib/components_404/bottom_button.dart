import 'package:flutter/material.dart';
import 'package:fchoolfood/screens/splash/splash_screen.dart';
import '../utils_404/my_strings.dart';

class BottomButton extends StatelessWidget {
  const BottomButton({
    Key? key,
    required this.size,
    required this.textTheme,
    required this.btnColor,
    required this.page,
  }) : super(key: key);

  final Size size;
  final TextStyle? textTheme;
  final Color btnColor;
  final Widget page;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0), // Đặt bán kính bo tròn tại đây
      ),
      minWidth: size.width / 1.2,
      height: size.height / 14,
      color: btnColor,
      onPressed: () {
        Navigator.pushNamed(context, SplashScreen.routeName);
      },
      child: Text(
        MyStrings.goHomeText,
        style: textTheme,
      ),


    );
  }
}
