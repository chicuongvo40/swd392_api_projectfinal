import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
class DropDown extends StatefulWidget {
  const DropDown({super.key});

  @override
  State<DropDown> createState() => _PaymentState();
}

class _PaymentState extends State<DropDown> {
  String dropdownvalue = "One";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 0),
      child: DropdownButton<String>(
        value: dropdownvalue,
        icon: Icon(Icons.menu),
        style: TextStyle(
            color: Colors.black
        ),
        underline: Container(
          height: 2,
          color: Colors.white,
        ),
        onChanged: (String? newValue){
          setState(() {
            dropdownvalue = newValue!;
          });
        },
        items: [
          DropdownMenuItem<String>(
            value: "One",
            child: Text("Class: 101"),
          ),
          DropdownMenuItem<String>(
            value: "Two",
            child: Text("Class: 102"),
          ),
          DropdownMenuItem<String>(
            value: "Three",
            child: Text("Class: 103"),
          ),
        ],
      ),
    );
  }
}

