import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:fchoolfood/models/Cart.dart';
import 'package:provider/provider.dart';
import 'components/body.dart';
import 'components/check_out_card.dart';

class CartScreen extends StatefulWidget {
  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {


  void parentMethod() {
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context)  {
    return Scaffold(
      body: Body(parentMethod: parentMethod),
      bottomNavigationBar: CheckoutCard( ),
    );
  }
  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      flexibleSpace: Container(
        color: Colors.white,
        child: Padding(
          padding: EdgeInsets.only(top: 10, left: 10, right: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.grey,
                  size: 32,
                ),
              ),

              Text("   Your Cart", style: TextStyle(color: Colors.black,fontSize: 28),),
              InkWell(
                onTap: () {},
                child: Icon(
                  CupertinoIcons.cart_fill,
                  color: Colors.grey,
                  size: 32,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
