// import "dart:convert";
//
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:fchoolfood/api/post.dart';
// import 'package:fchoolfood/api/postapi.dart';
//
// import 'package:fchoolfood/models/FoodProduct.dart';
//
// import 'package:http/http.dart' as http;
//
//
// List<Results> parseFoodProduct(String responseBody){
//   var list = jsonDecode(responseBody) as Map<String, dynamic>;
//   List<Results> product = List.empty();
//   print(list);
//   FoodProduct food = FoodProduct.fromJson(list);
//
//      product = food.results!;
//      print(product.length);
//       return product;
// }
// List<Resultss> parseItem(String responseBody){
//   var list = jsonDecode(responseBody) as Map<String, dynamic>;
//   List<Resultss> product = List.empty();
//   Post food = Post.fromJson(list);
//   product = food.resultss!;
//   return product;
// }
//
// Future<List<Results>> fetchResults() async{
//   final response = await http.get(Uri.parse('https://10.86.7.153:45456/api/v1.0/product'));
//   if(response.statusCode == 200){
//     print("Da chay api lay san pham");
//     return compute(parseFoodProduct, response.body);
//   }
//   else{
//     throw Exception('Request API Error');
//   }
//
// }
//
// Future<Item> createItem(Item a) async {
//   Map<String, String> headers = {
//     "Content-Type": "application/json",
//   };
//   final uri = Uri.parse("https://10.86.7.153:45456/api/v1.0/order/CreateOrder");
//   final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
//   if(response.statusCode == 200){
//     return Item.fromJson(json.decode(response.body));
//   }
//   else{
//     throw Exception('Request API Error' + response.statusCode.toString()  );
//   }
// }
// Future<List<Resultss>> fetchItem() async{
//   final response = await http.get(Uri.parse('https://10.86.7.153:45456/api/v1.0/order'));
//   if(response.statusCode == 200){
//     return compute(parseItem, response.body);
//   }
//   else{
//     throw Exception('Request API Error');
//   }
// }
// class ResultProvider with ChangeNotifier {
//   List<Results> _results = [];
//   void addData() {
//     fetchResults().then((value) => {
//       for (var item in value) {_results.add(item)},
//       notifyListeners()
//     });
//   }
//   ResultProvider() {
//     addData();
//   }
//   List<Results> get results => _results;
// }

import "dart:convert";

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fchoolfood/api/post.dart';
import 'package:fchoolfood/api/postapi.dart';

import 'package:fchoolfood/models/FoodProduct.dart';

import 'package:http/http.dart' as http;


List<Results> parseFoodProduct(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<Results> product = List.empty();
  FoodProduct food = FoodProduct.fromJson(list);
  product = food.results!;
  return product;
}
List<Resultss> parseItem(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<Resultss> product = List.empty();
  Post food = Post.fromJson(list);
  product = food.resultss!;
  return product;
}

Future<List<Results>> fetchResults() async{
  final response = await http.get(Uri.parse('https://webapp-ffpt.azurewebsites.net/api/v1.0/product'));
  if(response.statusCode == 200){

    return compute(parseFoodProduct, response.body);
  }
  else{
    throw Exception('Request API Error');
  }
}

Future<Item> createItem(Item a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://webapp-ffpt.azurewebsites.net/api/v1.0/order/CreateOrder");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  if(response.statusCode == 200){
    return Item.fromJson(json.decode(response.body));
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString() );
  }
}
Future<List<Resultss>> fetchItem() async{
  final response = await http.get(Uri.parse('https://webapp-ffpt.azurewebsites.net/api/v1.0/order'));
  if(response.statusCode == 200){
    return compute(parseItem, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}
class ResultProvider with ChangeNotifier {
  List<Results> _results = [];
  void addData() {
    fetchResults().then((value) => {
      for (var item in value) {_results.add(item)},
      notifyListeners()
    });
  }
  ResultProvider() {
    addData();
  }
  List<Results> get results => _results;
}